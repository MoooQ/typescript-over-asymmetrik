import { Capability, ISearchCapability, Server, AbstractServer, IServerOptions, IServerStatus } from "../src/index";
import { CapabilitiesModule } from "../src/capability/capability-bootstrap";
import { Observable } from "rxjs";

import { Server as Static } from "../src/server-bootstrap";

import { R4 } from '@ahryman40k/ts-fhir-types';

@Capability({
    capability: ['search'],
    resource: "patient",
    criterias: ['identifier', 'name']

})
class PatientSearchCapability implements ISearchCapability<R4.IPatient> {


    async search(): Promise<R4.IPatient[]> {
        console.log(`====>   Ask to search patients`);
        return [{
            "resourceType": "Patient",
            "id": "example"
        }];

    }

    async searchById( query: { id: string }): Promise<R4.IPatient> {
        console.log(`====>   Ask to search a patient, ${query.id}`);
        return {
            "resourceType": "Patient",
            "id": query.id
        };
    }

    async searchByVersionId(args: any, context: any) {
        console.log(`====>   Ask to searchByVersionId a patient, ${args}, ${context}`);
    }

    async create(obj: R4.IPatient) {
    }

    async update(obj: R4.IPatient) {
    }

    async remove(query: { id: string }) {
    }

    async patch() {
    }

    async history(query: { id: string }) {
    }


    async historyById(query: { id: string }) {
    }

}



@Capability({
    capability: ['history'],
    resource: "procedure",
    criterias: ['identifier', 'name']

})
class ProcedureSearchCapability implements ISearchCapability<R4.IProcedure> {
    async search(): Promise<R4.IProcedure[]> {
        console.log('====>   Ask to search a procedure');
        return [];
    }
    async searchById(query: { id: string }): Promise<R4.IProcedure> {
        console.log('====>   Ask to searchBy a patient');
        return {
            "resourceType": "Procedure",
            "subject": {

            }
        };
    }
}




@Server({
    capabilities: [
        PatientSearchCapability,
        ProcedureSearchCapability
    ]
})
class MyServer extends AbstractServer {
    constructor(capabilities: CapabilitiesModule) {
        super(capabilities);
    }

    start(options: IServerOptions): Observable<IServerStatus> {
        return super.start(options);
    }

    stop(): void {
        return super.stop();
    }
}


Static.Bootstrap(MyServer)
    .then(server => {
        server.start({ port: 3001 });
        console.log('server is running on port 3001')
    });