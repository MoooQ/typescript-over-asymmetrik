# Introduction
Library based on Asymmetrik [node-fhir-server-core library](https://github.com/Asymmetrik/node-fhir-server-core)  
Thanks to it, I'm proud to offer to Typescript community a fully typed typescript FHIR server !


> As asymmetrik still not accepted my [pull request](https://github.com/Asymmetrik/node-fhir-server-core/pull/213), I must use a fork.  
> You can find sources of that fork [here](https://github.com/Ahryman40k/asymmetrik-sample/commits/master)




[![codecov](https://codecov.io/gl/healthcare-os/typescript-over-asymmetrik/branch/master/graph/badge.svg?token=29OvkoWCj8)](https://codecov.io/gl/healthcare-os/typescript-over-asymmetrik)



# How it's work
If you are familiar with Angular framework, you will understand quickly how thing works.

> You can also find a working exemple in the sample folder

## 1.Implement the server

A base class is present to help you to quickly define your own server. I mean:
*  Add custom middleware
*  Add additional routes to FHIR server

But in the most basic use, you have to define a server object as following
```typescript
class MyServer extends AbstractServer {
    constructor(capabilities: CapabilitiesModule) {
        super(capabilities);
    }

    start(options: IServerOptions): Observable<IServerStatus> {
        return super.start(options);
    }

    stop(): void {
        return super.stop();
    }
}
``` 

## 2.Implement FHIR resources class
Now we can create classes to handle a fhir request on specific resource 
```typescript
class MyPatient implements ISearchCapability {
    search(): any {

    }
}
```

Fine, the most difficult is done !

## 3.Add decorators 
Now we need to add some decorations on our resource class to tell the library how to use it.
In the following exemple I will add Capability to my object to:
*  define a search capability in server's metadata
*  set kind of FHIR resource this object will handle
*  define which search parameters should be used with this object

```typescript
@Capability({
    capability: ['search'],
    resource: "patient",
    criterias: ['identifier', 'name']

})
class MyPatient implements ISearchCapability {
    [...]
}
```

we also decorate our server to let it know about our capabilities
```typescript
@Server({
    capabilities: [
        MyPatient
    ]
})
class MyServer extends AbstractServer {
    [...]
}
```

## 4.Bootstrap the server
Finally we call a bootstrapping function that will set up the asymmetrik server for us.
```typescript
Static.Bootstrap(MyServer)
    .then(server => {
        server.start({ port: 3001 });
        console.log('server is running on port 3001')
    });
``` 

No more step is required, you can now focus on how resolve your search request !



