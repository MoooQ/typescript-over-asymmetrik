export type CapabilityKind = 'search' | 'create' | 'update' | 'remove' | 'history';
