
  

export interface IServerExtendedBehaviour {
    applyMiddlewares(app: any): void;
    initializeRoutes(app: any): void;
}
