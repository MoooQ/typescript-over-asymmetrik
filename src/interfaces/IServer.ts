
import { Observable } from 'rxjs';
import { IServerOptions } from './IServerOptions';
import { IServerStatus } from './IServerStatus';


export interface IServer extends Readonly<{
    httpServer: any // Can't use readonly keywork with interface, so inherit readonly properties like this
}> {

    start(options: IServerOptions): Observable<IServerStatus>;
    stop(): void;

}