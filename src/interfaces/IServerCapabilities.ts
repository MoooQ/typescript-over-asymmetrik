
import { FhirResourceName, CapabilityKind } from "../helpers";

export interface ICapability {

}

export interface ISearchCapability<T> extends ICapability {

    search(): Promise<T[]>;
    searchById(query: { id: string }): Promise<T>;
}

export function isISearchCapability<T>(object: any ): object is ISearchCapability<T> {

    const fhirResult = FhirResourceName.decode( object.resource )
    return  fhirResult.isRight() && object.capability.includes('search');
}

export interface ICreateCapability<T> extends ICapability {
    create(obj: T): Promise<T>;
}

export function isICreateCapability<T>(object: any): object is ICreateCapability<T> {
    const fhirResult = FhirResourceName.decode( object.resource )
    return  fhirResult.isRight() && object.capability.includes('create');
}

export interface IUpdateCapability<T> extends ICapability {
    update(obj: T): Promise<T>;
}

export function isIUpdateCapability<T>(object: any): object is IUpdateCapability<T> {
    const fhirResult = FhirResourceName.decode( object.resource )
    return  fhirResult.isRight() && object.capability.includes('update');
}


export interface IRemoveCapability<T> extends ICapability {
    remove(query: { id: string }): Promise<T>
}

export function isIRemoveCapability<T>(object: any): object is IRemoveCapability<T> {
    const fhirResult = FhirResourceName.decode( object.resource )
    return  fhirResult.isRight() && object.capability.includes('remove');
}



export interface IHistoryCapability<T> extends ICapability {
    historyById(query: { id: string }): Promise<T>;
}

export function isIHistoryCapability<T>(object: any): object is IHistoryCapability<T> {
    const fhirResult = FhirResourceName.decode( object.resource )
    return  fhirResult.isRight() && object.capability.includes('history');
}



