export interface IServerStatus {
    port?: number;
    state: 'not started' | 'started' | 'closed';
}
