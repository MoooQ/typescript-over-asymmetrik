import { IServerExtendedBehaviour } from './IServerExtendedBehaviour'

export interface IServerOptions {
    port: number;
    serverBehavior?: IServerExtendedBehaviour;
    
}
