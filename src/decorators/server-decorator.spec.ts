import "reflect-metadata"
import { metadataKeyServer, ServerModuleSettings, Server } from "../decorators"


// @Capapility( 'search' )
// class SubjectSearchCapability {
//     name = 'subject';
//     filter = `resource->'subject'->>'reference' LIKE $`;
//     bind = (value: any) => value;
// }
//
// @server({
//     capabilities: [
//         SubjectSearchCapability,
//         ...
//     ]
// })
// export class Server implements AbstractServer {
//     ...
// }

class Testable {
}

describe( 'Server decorator', () => {

    afterEach( () => {
        Reflect.deleteMetadata( metadataKeyServer, Testable );
    })


    test( 'Should define metadatada', () => {
        const settings: ServerModuleSettings = { capabilities: [] }
        const decorator = Server( settings );
        decorator( Testable );

        const meta = Reflect.getMetadata( metadataKeyServer, Testable );
        expect( meta ).toBeDefined();
        expect( meta ).toBe( settings  );
    })

})
