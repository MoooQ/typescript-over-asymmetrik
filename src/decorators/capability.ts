import { Type } from '../interfaces/type';
import { CapabilityKind } from '../helpers/CapabilityKind';
import { FhirResourceName } from '../helpers/FhirResourceName';

export const metadataKeyCapability = Symbol('Capability');

export type CapabilityDecoratorOptions = {
    capability: CapabilityKind[],
    resource: FhirResourceName,
    criterias: string[]
}

export function Capability(
    options: CapabilityDecoratorOptions
): (target: Type<any>) => void {

    return (ctor: Type<any>) => {

        Reflect.defineMetadata(metadataKeyCapability, options, ctor);
        return class extends ctor {
            resource = options.resource;
            capability = options.capability;
        };
    }
}
