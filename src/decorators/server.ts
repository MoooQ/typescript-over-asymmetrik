import { Type, ISearchCapability, ICapability } from "../interfaces";



export interface IServerModuleSettings {
  capabilities: Type<ICapability>[];
}

export class ServerModuleSettings implements IServerModuleSettings {
  capabilities: Type<ICapability>[] = [];
}

export const metadataKeyServer = Symbol('FHIR.Server.Module');

export function Server(
  settings: ServerModuleSettings
): (target: Type<any>) => void {

  return (ctor: Type<any>) => {
    Reflect.defineMetadata(metadataKeyServer, settings, ctor);
    return class extends ctor {
      constructor(private capabilities) {
        super( capabilities );
      }
    }
  }
}

