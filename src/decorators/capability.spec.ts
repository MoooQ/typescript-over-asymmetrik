import "reflect-metadata"
import { metadataKeyCapability, CapabilityDecoratorOptions, Capability } from "../decorators"

// @Capapility( 'search' )
// class SubjectSearchCapability {
//     name = 'subject';
//     filter = `resource->'subject'->>'reference' LIKE $`;
//     bind = (value: any) => value;
// }

class Testable {

}

describe ( 'Search capabilities', () => {
    afterEach( () => {
        Reflect.deleteMetadata( metadataKeyCapability, Testable );
    })

    
    test( 'Should define metadata', () => {

        const settings: CapabilityDecoratorOptions = {
            capability: ['search'],
            resource: 'patient',
            criterias: ['subject']
        } 

        const decorator = Capability(settings);
        decorator( Testable );

        const meta = Reflect.getMetadata( metadataKeyCapability, Testable );
        expect( meta ).toBeDefined();
        expect( meta ).toStrictEqual( settings  );

    })
})

