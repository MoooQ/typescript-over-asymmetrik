import dotenv from 'dotenv';
dotenv.config();

import { IServer, IServerOptions, IServerStatus } from "./interfaces";
import { Observable, of } from "rxjs";

import FHIRServer from '@ahryman40k/node-fhir-server-core';
import { CapabilitiesModule } from './capability/capability-bootstrap';

import * as _ from 'lodash';
import { CapabilityAdapter } from './capability/CapabilityAdapter';


// This is an extending type function
declare module './capability/capability-bootstrap' {
    interface CapabilitiesModule {
        toAsymmetrikConfig(): any;
    }
}




export abstract class AbstractServer implements IServer {

    private _server;
    private _logger;

    constructor(private _capabilities: CapabilitiesModule) {
        CapabilitiesModule.prototype.toAsymmetrikConfig = () => {

            const profiles = _.keys(this._capabilities.capabilities).map(key => {
                let o = {};
                o[key] = {
                    service: new CapabilityAdapter(_capabilities.capabilities[key].instance),
                    versions: ['4_0_0'],
                    metadata: {
                        makeResource: (args: any, context: any) => _capabilities.makeResource(args, context)
                    }
                };
                return o;
            })

            return {
                profiles: Object.assign({}, ...profiles)
            }
        }
    }

    get httpServer(): any {
        return this._server.app;
    }

    start(options: IServerOptions): Observable<IServerStatus> {

        this._server = FHIRServer.initialize(this._capabilities.toAsymmetrikConfig());
        this._logger = FHIRServer.loggers.get('default');

        this._server.listen(options.port)
        return of({ state: 'started', port: options.port })
    }

    stop(): void {
    }


}