import "reflect-metadata";
import { Server, Capability } from "./decorators"
import { IServerStatus, IServerOptions, ISearchCapability } from "./interfaces";
import { AbstractServer } from "./server"
import { Observable } from 'rxjs';
import { CapabilityEngine } from "./capability/capability-bootstrap";


import { R4 } from '@ahryman40k/ts-fhir-types';

//**********************************************
//********** Test Case *************************
//**********************************************
@Capability({
    capability: ['search'],
    resource: "patient",
    criterias: ['subject']
})
class SubjectSearchCapability implements ISearchCapability<R4.IPatient> {

    async search(): Promise<R4.IPatient[]> {
        return [];
    }

    async searchById(query: { id: string }): Promise<R4.IPatient> {
        return { resourceType: "Patient" };
    }
}


@Server({
    capabilities: [
        SubjectSearchCapability
    ]
})
class MyServer extends AbstractServer {
    constructor(module) {
        super(module);
    }

    start(options: IServerOptions): Observable<IServerStatus> {
        return super.start(options);
    }

    stop(): void {
        return super.stop();
    }
}

//**********************************************


describe('Server Behavior', () => {

    test('Should should have status "started"', () => {
       /* const port = 4200;
        let server: MyServer;
        return CapabilityEngine.Bootstrap([SubjectSearchCapability])
            .then(m => {
                server = new MyServer(m);
                return server.start({ port }).toPromise();
            })
            .then(status => {
                expect(status.port).toBe(port);
                expect(status.state).toBe('started');
                server.stop();
            })*/
    })
})

