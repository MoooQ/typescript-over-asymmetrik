import "reflect-metadata";
import { metadataKeyServer, ServerModuleSettings, Server, Capability } from "./decorators"
import { IServerStatus, IServerOptions, ISearchCapability, IUpdateCapability, IHistoryCapability } from "./interfaces";
import { AbstractServer } from "./server"
import { Server as Static } from "./server-bootstrap";
import { Observable } from 'rxjs';

import * as path from 'path';
import { CapabilitiesModule } from "./capability/capability-bootstrap";


import { R4 } from '@ahryman40k/ts-fhir-types';

// import supertest as request from 'supertest';

//*************************************************************
//************************* Test Case *************************
//*************************************************************
@Capability({
    capability: ['search', 'history'],
    resource: "patient",
    criterias: ['identifier', 'name']

})
class PatientSearchCapability implements ISearchCapability<R4.IPatient>, 
                                         IHistoryCapability<R4.IPatient> {
    

    async search( ) : Promise<R4.IPatient[] >{
  /*      const Procedure = await import(FHIRServer.resolveSchema(args.base_version, 'procedure'));
        const builder = new R4.R4QueryBuilder(implementedSearchCriterias, dataSourceRequests);
         const query = builder.build('search', 'Patient', { procedureId: args.id });*/

         return [];
    }

    async searchById(query: {id: string}) :Promise<R4.IPatient>{
        return { resourceType: "Patient"};
    }

    async historyById(query: { id: string }): Promise<R4.IPatient>  {
        return { resourceType: "Patient"};
    }

    
}


@Server({
    capabilities: [
        PatientSearchCapability
    ]
})
class MyServer extends AbstractServer {
    constructor( capabilities: CapabilitiesModule) {
        super( capabilities );
    }

    start(options: IServerOptions): Observable<IServerStatus> {
        return super.start(options);
    }

    stop(): void {
        return super.stop();
    }
}

//**********************************************

describe('Server bootstrap', () => {
    test('Should return a server', () => {
     /*   return Static.Bootstrap(MyServer)
            .then(server => {
                server.start({ port: 3001 });
                expect(server).toBeInstanceOf(MyServer)
            });*/
    })


})