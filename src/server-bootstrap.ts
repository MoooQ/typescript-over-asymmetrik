import { Type } from "./interfaces/type";
import { metadataKeyServer, ServerModuleSettings } from "./decorators";
import { AbstractServer } from "./server";
import { CapabilityEngine } from "./capability/capability-bootstrap";


export class Server {

    public static async Bootstrap( target: Type<AbstractServer> ): Promise<AbstractServer> {
        

        const settings  = Reflect.getMetadata( metadataKeyServer, target) as ServerModuleSettings;
        if (!settings) {
            throw new Error('Server decorator is required to bootstrap object');
        }

        const module = await CapabilityEngine.Bootstrap( settings.capabilities );
        const baseObject = new target(module); 
        

        return baseObject;
    }
}