import { isISearchCapability, isICreateCapability, isIUpdateCapability, isIRemoveCapability, isIHistoryCapability } from "../interfaces";
import FHIRServer from '@ahryman40k/node-fhir-server-core';
import { R4 } from '@ahryman40k/ts-fhir-types';
export class CapabilityAdapter<D> {
    constructor(private _decorated: D) {
    }
    async search(args: any, context: any): Promise<R4.IBundle> {

        if (isISearchCapability(this._decorated)) {
            try {
                const results = await this._decorated.search();
                return <R4.IBundle>{
                    resourceType: 'Bundle',
                    total: results.length,
                    type: 'searchset',
                    entry: results.map(r => ({
                        fullurl: 'http(s)://to.be.determined',
                        search: {
                            'mode': "match"
                        },
                        resource: r,
                        meta: {
                            lastUpdated: (new Date(Date.now())).toUTCString()
                        },
                    })),
                };
            }
            catch (ex) {
                throw new FHIRServer.ServerError(`search operation failed`, {
                    statusCode: 500,
                    issue: [{
                        severity: 'error',
                        code: 'exception'
                    }]
                });
            }
        }

        throw new FHIRServer.ServerError(`Invalid url - no search capability defines for that resource`, {
            statusCode: 404,
            issue: [{
                severity: 'error',
                code: 'not-supported',
            }]
        });

    }
    async searchById(args: any, context: any): Promise<any> {
        try {
            if (isISearchCapability(this._decorated)) {
                const { id } = args;
                const result = await this._decorated.searchById({ id });

                if (result) {
                    return result;
                }

                throw new FHIRServer.ServerError(`resource id: ${id} was not found`, {
                    statusCode: 200,
                    issue: [{
                        severity: 'information',
                        code: 'not-found',
                    }]
                });
            }
            throw new FHIRServer.ServerError(`Invalid url - no search capability defines for that resource`, {
                statusCode: 404,
                issue: [{
                    severity: 'error',
                    code: 'not-supported',
                }]
            });
        }
        catch {
            throw new FHIRServer.ServerError(`search operation failed`, {
                statusCode: 500,
                issue: [{
                    severity: 'error',
                    code: 'exception'
                }]
            });
        }
    }
    async create(args: any, context: any): Promise<any> {
        try {
            if (isICreateCapability(this._decorated)) {
                const body = context.body;
                return await this._decorated.create({ body });
            }
            throw new FHIRServer.ServerError(`Invalid url - no create capability defines for that resource`, {
                statusCode: 404,
                issue: [{
                    severity: 'error',
                    code: 'not-supported',
                }]
            });
        }
        catch {
            throw new FHIRServer.ServerError(`create operation failed`, {
                statusCode: 500,
                issue: [{
                    severity: 'error',
                    code: 'exception'
                }]
            });
        }
    }
    async update(args: any, context: any): Promise<any> {
        try {
            if (isIUpdateCapability(this._decorated)) {
                const body = context.body;
                return await this._decorated.update({ body });
            }
            throw new FHIRServer.ServerError(`Invalid url - no update capability defines for that resource`, {
                statusCode: 404,
                issue: [{
                    severity: 'error',
                    code: 'not-supported',
                }]
            });
        }
        catch (ex) {
            throw new FHIRServer.ServerError(`update operation failed`, {
                statusCode: 500,
                issue: [{
                    severity: 'error',
                    code: 'exception'
                }]
            });
        }
    }
    async remove(args: any, context: any): Promise<any> {
        try {
            if (isIRemoveCapability(this._decorated)) {
                const { id } = args;
                return await this._decorated.remove({ id });
            }
            throw new FHIRServer.ServerError(`Invalid url - no remove capability defines for that resource`, {
                statusCode: 404,
                issue: [{
                    severity: 'error',
                    code: 'not-supported',
                }]
            });
        }
        catch (ex) {
            throw new FHIRServer.ServerError(`remove operation failed`, {
                statusCode: 500,
                issue: [{
                    severity: 'error',
                    code: 'exception'
                }]
            });
        }
    }
    async historyById(args: any, context: any): Promise<any> {

        if (isIHistoryCapability(this._decorated)) {
            const { id } = args;
            try {
                const result = await this._decorated.historyById({ id });

                if (result) {
                    return result;
                }
            }
            catch (ex) {
                throw new FHIRServer.ServerError(`history operation failed`, {
                    statusCode: 500,
                    issue: [{
                        severity: 'error',
                        code: 'exception'
                    }]
                });
            }

            throw new FHIRServer.ServerError(`resource id: ${id} was not found`, {
                statusCode: 200,
                issue: [{
                    severity: 'information',
                    code: 'not-found',
                }]
            });

        }
        throw new FHIRServer.ServerError(`Invalid url - no history capability defines for that resource`, {
            statusCode: 404,
            issue: [{
                severity: 'error',
                code: 'not-supported',
            }]
        });

    }
}
