import "reflect-metadata"
import { CapabilityAdapter } from "./CapabilityAdapter";
import { ISearchCapability, IHistoryCapability } from "../interfaces/IServerCapabilities";
import { R4 } from '@ahryman40k/ts-fhir-types';

const patient0: R4.IPatient = {
            "resourceType": "Patient",
            "id": "example"
        }

class Testable implements ISearchCapability<R4.IPatient>, IHistoryCapability<R4.IPatient> {
    async search(): Promise<R4.IPatient[]> {
        return [patient0];
    }

    async searchById( query: {id: string} ): Promise<R4.IPatient> {
        return patient0;
    }

    async historyById( query: {id: string} ): Promise<R4.IPatient> {
        return patient0;
    }
}




describe('CapabilityAdapter', () => {
    describe('Search', () => {

        test('Should call search method on ISearchCapability Object', () => {
            const testable = Object.assign( new Testable(), { resource: 'patient', capability: ['search']});
            const decorated = new CapabilityAdapter(testable);

            return decorated.search({}, {}).then( (r: R4.IBundle) => {
                expect(r.resourceType).toBe('Bundle');
                expect(r.type).toBe('searchset');
                expect(r.total).toBe(1);
                expect(r.entry![0].resource).toBe(patient0);
            });
        }),
        
        test('Should not call search method if object doesn\'t inherit from ISearchCapability', () => {
            const testable = Object.assign( new Testable(), { resource: 'patient', capability: ['history']});
            const decorated = new CapabilityAdapter(testable);

            return decorated.search({},{})
                .then( r => {
                    expect(true).toBeFalsy();
                }).catch( err => {
                    expect(err.statusCode).toBe(404);
                    expect(err.message).toMatchSnapshot();
                });
        })

    }),

    describe('History', () => {

        test('Should call history method on IHistoryCapability Object', () => {
            const testable = Object.assign( new Testable(), { resource: 'patient', capability: ['history']});
            const decorated = new CapabilityAdapter(testable);

            return decorated.historyById({}, {}).then( (r: R4.IPatient) => {
                
                expect(true).toBe(true);
            });
        }),
        
        test('Should not call history method if object doesn\'t inherit from IHistoryCapability', () => {
            const testable = Object.assign( new Testable(), { resource: 'patient', capability: ['search']});
            const decorated = new CapabilityAdapter(testable);

            return decorated.historyById({},{})
                .then( r => {
                    expect(true).toBeFalsy();
                }).catch( err => {
                    expect(err.statusCode).toBe(404);
                    expect(err.message).toMatchSnapshot();
                });
        })

    })
})