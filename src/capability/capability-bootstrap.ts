import { CapabilityDecoratorOptions, metadataKeyCapability } from "../decorators";
import DecoratorException from "../errors/DecoratorException";
import FhirResourceException from "../errors/FHIRResourceException";
import { Type } from "../interfaces/type";
import { AsymmetrikParameters, AsymmetrikParametersByResources } from "../asymmetrik/parameters.type";

export class CapabilitiesModule {

    constructor(private _capabilities: AsymmetrikParametersByResources) {

    }

    get capabilities(): AsymmetrikParametersByResources {
        return this._capabilities;
    }

    makeResource = (args: { base_version: string, key: string }, logger: any) => {
        return {
            type: args.key,
            profile: {
                reference: `http://hl7.org/fhir/${args.key}.html`
            },
            searchParam:  Object.values(this.capabilities[args.key].searchParam)
            
        };
    }
}

export class CapabilityEngine {
    public static async Bootstrap(target: Type<any>[]): Promise<CapabilitiesModule> {

        const pc = target.map(async t => {
            const baseObject = new t(); // My capability object

            const settings = Reflect.getMetadata(metadataKeyCapability, t) as CapabilityDecoratorOptions;
            if (!settings) {
                throw new DecoratorException('Capability');
            }

            try {
                // use jspath only in production mode (not in tests or debug). 
                // Can be a little bit messy with minification/uglyfication
                const tsPath =  `../asymmetrik/R4_0_0/${settings.resource}.parameters.js.ts`;
                const jsPath =  `../asymmetrik/R4_0_0/${settings.resource}.parameters.js.js`; 
                
                let module;
                try {
                    module = await import(jsPath); // In dist
                } catch {
                    module = await import(tsPath); // When debugging and testing
                }

                const capObjs: AsymmetrikParameters = Object.keys(module.parameters)
                    .filter(key => settings.criterias.includes(key))
                    .reduce((obj, key) => {
                        obj[key] = Object.assign({}, { name: key }, module.parameters[key]);
                        return obj;
                    }, {});
                let o: AsymmetrikParametersByResources = {};
                o[settings.resource] = Object.assign({}, { searchParam: capObjs }, { instance: baseObject });
                return o;
            } catch (err) {
                throw new FhirResourceException(settings.resource);
            }

        })

        try {
            const results = await Promise.all(pc);
            return new CapabilitiesModule(Object.assign({}, ...results));
        } catch (err) {
            throw err;
        }

    }
}