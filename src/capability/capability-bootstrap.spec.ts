import "reflect-metadata"
import { metadataKeyCapability, CapabilityDecoratorOptions, Capability } from "../decorators"
import { CapabilityEngine } from "./capability-bootstrap"
import FhirResourceException from "../errors/FHIRResourceException"
import { thisExpression } from "@babel/types"
import DecoratorException from "../errors/DecoratorException"

// @Capapility( 'search' )
// class SubjectSearchCapability {
//     name = 'subject';
//     filter = `resource->'subject'->>'reference' LIKE $`;
//     bind = (value: any) => value;
// }

class Testable {

}



describe('Capability Engine', () => {
    describe('Bootstrap', () => {

        afterEach(() => {
            Reflect.deleteMetadata(metadataKeyCapability, Testable);
        })

        test('Should return capabilityModule object', () => {
            const settings: CapabilityDecoratorOptions = {
                capability: ['search'],
                resource: 'patient',
                criterias: ['identifier', 'name', 'phone']
            }

            const decorator = Capability(settings);
            decorator(Testable);

            return CapabilityEngine.Bootstrap( [Testable] ).then( m => {
                expect(m).toBeDefined();
                expect( Object.keys(m.capabilities).length ).toBe(1);
            });
            
        })

        test('Should throw on invalid FHIR resource name', () => {
            const settings: CapabilityDecoratorOptions = {
                capability: ['search'],
                resource: 'hvs' as 'patient',
                criterias: ['subject']
            }

            const decorator = Capability(settings);
            decorator(Testable);
            return CapabilityEngine.Bootstrap( [Testable] )
                .then( r => expect(true).toBeFalsy() )
                .catch( err => {
                    expect(err).toBeInstanceOf(FhirResourceException);
                    expect(err.message).toMatchSnapshot();
                })
        })

        test('Should accept decorated object only', () => {
            return CapabilityEngine.Bootstrap( [Testable] )
                .then( r => expect(true).toBeFalsy() )
                .catch( err => {
                    expect(err).toBeInstanceOf(DecoratorException);
                    expect(err.message).toMatchSnapshot();
                })
        })

        test('Should works with empty target', () => {
            return CapabilityEngine.Bootstrap([])
                .then( m => {
                    expect( Object.keys(m.capabilities).length ).toBe(0);
                })
        })
    })

    describe( 'CapabilitiesModule', () => {
        test('Should produce an Asymmetrik capability object', () => {
            const settings: CapabilityDecoratorOptions = {
                capability: ['search'],
                resource: 'patient',
                criterias: ['identifier', 'name', 'phone']
            }

            const decorator = Capability(settings);
            decorator(Testable);

            return CapabilityEngine.Bootstrap( [Testable] )
                .then( m => {
                    const patient = m.capabilities['patient'];
                    expect(patient).toBeDefined();
                    expect(patient.instance).toBeDefined();
                    const searchParams = patient['searchParam'];
                    expect(searchParams).toBeDefined();
                    expect( searchParams.identifier).toBeDefined();
                    expect( searchParams.name).toBeDefined();
                    expect( searchParams.phone).toBeDefined();
                });
        })


        test('Should produce a metadata fragment for a resource kind', () => {
            const settings: CapabilityDecoratorOptions = {
                capability: ['search'],
                resource: 'patient',
                criterias: ['identifier', 'name', 'phone']
            }

            const decorator = Capability(settings);
            decorator(Testable);

            return CapabilityEngine.Bootstrap( [Testable] )
                .then( m => {
                    const fragment = m.makeResource( { base_version: '4.0.1', key: 'patient'}, {});
                    expect( fragment ).toMatchSnapshot();
                });
        })
    })
})