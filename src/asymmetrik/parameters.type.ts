
import { FhirResourceName } from '../helpers/FhirResourceName'

export type AsymmetrikParameter = {
    type: string;
    fhirtype: string;
    xpath: string;
    definition: string;
    description:string;
    name?: string;
}

export type AsymmetrikParameters = { [key: string]: AsymmetrikParameter };

export type AsymmetrikParametersByResources = { [P in keyof FhirResourceName ]?: AsymmetrikParameters }

