/**
 * @name exports
 * @static
 * @summary Arguments for the verificationresult query
 */
import { AsymmetrikParameters } from '../parameters.type';
export const parameters: AsymmetrikParameters = {
	target: {
		type: 'reference',
		fhirtype: 'reference',
		xpath: 'VerificationResult.target',
		definition: 'http://hl7.org/fhir/SearchParameter/VerificationResult-target',
		description: 'A resource that was validated',
	},
};
