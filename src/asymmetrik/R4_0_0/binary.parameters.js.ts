/**
 * @name exports
 * @static
 * @summary Arguments for the binary query
 */
import { AsymmetrikParameters } from '../parameters.type';
export const parameters: AsymmetrikParameters = {
	contenttype: {
		type: 'token',
		fhirtype: 'token',
		xpath: 'Binary.contentType',
		definition: 'http://hl7.org/fhir/SearchParameter/Binary-contenttype',
		description: 'MimeType of the binary content',
	},
};
