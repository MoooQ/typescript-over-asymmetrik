/**
 * @name exports
 * @static
 * @summary Arguments for the substancespecification query
 */
import { AsymmetrikParameters } from '../parameters.type';
export const parameters: AsymmetrikParameters = {
	code: {
		type: 'token',
		fhirtype: 'token',
		xpath: 'SubstanceSpecification.code',
		definition: 'http://hl7.org/fhir/SearchParameter/SubstanceSpecification-code',
		description: 'Codes associated with the substance',
	},
};
