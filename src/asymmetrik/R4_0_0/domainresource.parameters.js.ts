/**
 * @name exports
 * @static
 * @summary Arguments for the domainresource query
 */
import { AsymmetrikParameters } from '../parameters.type';
export const parameters: AsymmetrikParameters = {
	_text: {
		type: 'string',
		fhirtype: 'string',
		xpath: '',
		definition: 'http://hl7.org/fhir/SearchParameter/DomainResource-text',
		description: 'Search on the narrative of the resource',
	},
};
