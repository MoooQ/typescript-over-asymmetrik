import AhryException from "./AhryException";

export default class DecoratorException extends AhryException {
    constructor(private decoratorName: string) {
      super( `A valid decorator: ${decoratorName} is required` );
    }
  }