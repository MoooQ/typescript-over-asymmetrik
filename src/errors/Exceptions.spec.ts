import FhirResourceException from "./FHIRResourceException"
import DecoratorException from "./DecoratorException";

describe('Exceptions', () => {

    test('DecoratorException', () => {
        const ex1 = new DecoratorException( 'Capability');
        const ex2 = new DecoratorException( 'Server');

        expect(ex1.message).toMatchSnapshot();
        expect(ex2.message).toMatchSnapshot();
    })

    test('FHIRResourceException', () => {
        const ex1 = new FhirResourceException( 'patient');
        const ex2 = new FhirResourceException( 'practitioner');

        expect(ex1.message).toMatchSnapshot();
        expect(ex2.message).toMatchSnapshot();

    })

})
