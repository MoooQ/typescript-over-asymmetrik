export default class AhryException extends Error {
    constructor(message) {
      super(message);
      this.name = this.constructor.name;
    }
  }