import AhryException from "./AhryException";

export default class FhirResourceException extends AhryException {
    constructor(private resourceName: string) {
      super( `Resource: ${resourceName} isn't a valid FHIR resource` );
    }
  }